package com.example.retrofit1;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class My_R_V_Adapter extends RecyclerView.Adapter<My_R_V_Adapter.Myholder> {

    private Context context;
    private List<Comment> messageList;


    public void setItems(List<Comment> messageList) {
        this.messageList = messageList;
        notifyDataSetChanged();
    }

    public My_R_V_Adapter(Context context) {
        this.context = context;

    }

    @NonNull
    @Override
    public Myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item, parent, false);
        Myholder myholder = new Myholder(view);
        return myholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Myholder holder, int position) {
        Comment comment = messageList.get(position);
        holder.bind(comment);
    }

    @Override
    public int getItemCount() {
        return  messageList == null ? 0 : messageList.size();
    }

    static public class Myholder extends RecyclerView.ViewHolder {

        TextView textViewID;
        TextView textViewUserID;
        TextView textViewUserName;
        TextView textViewUserEmail;
        TextView textViewUserText;


        public Myholder(@NonNull View itemView) {
            super(itemView);
            textViewID = itemView.findViewById(R.id.textViewID);
            textViewUserID = itemView.findViewById(R.id.textViewUserID);
            textViewUserName = itemView.findViewById(R.id.textViewUserName);
            textViewUserEmail = itemView.findViewById(R.id.textViewUserEmail);
            textViewUserText = itemView.findViewById(R.id.textViewUserText);


        }

        public void bind(final Comment comment) {
            String id = String.valueOf(comment.getId());
            String idUser = String.valueOf(comment.getPostId());
           textViewID.setText("ID: " + id);
           textViewUserID.setText("User ID: " + idUser);
           textViewUserName.setText("Name: " + comment.getName());
           textViewUserEmail.setText("Email: "+ comment.getEmail());
           textViewUserText.setText("Text: " + comment.getText());


        }

    }
}
